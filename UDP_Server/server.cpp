#ifndef _WIN32
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdexcept>
#include <arpa/inet.h>
#include <netdb.h>
#else
#include <winsock2.h>
#include <ws2tcpip.h>
#endif

#include <cstdlib>
#include <cstring>
#include <iostream>

#define PORT 8001
#define BUFFER_SIZE 512

#ifndef _WIN32
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#endif

/* Эта функция указывает версию WinSock и настраивает "behind the scenes stuff",
 * которые нужны для использования сокетов приложению (если Windows), в
 * противном случае просто возвращает true, так как на других ОС на этом этапе
 * настраивать ничего не нужно*/
bool initWSA() {
#ifdef _WIN32
    WSADATA wsaData;
    return WSAStartup(MAKEWORD(2, 2), &wsaData) == NO_ERROR;
#else
    return true;
#endif
}

/* Эта функция выполняет возврат кода ошибки*/
int getErrorCode(int errorCodeOtherOS) {
#ifdef _WIN32
    return WSAGetLastError();
#else
    return errorCodeOtherOS;
#endif
}

/* Следующая функция в случае с Windows закрывает указанный сокет и удаляет
 * структуры, инициализированные WSAStartup. Другие ОС просто закрывают сокет*/
int closeSocket(int udpSocketServer) {
#ifdef _WIN32
    int iCloseSocket = closesocket(udpSocketServer);
    if (iCloseSocket == SOCKET_ERROR) {
        std::cerr << "Socket closing failed & Error Code -> "
                  << getErrorCode(SOCKET_ERROR) << std::endl;
        return SOCKET_ERROR;
    } else {
        std::cout << "Socket closing success\n";

        int iWSACleanup = WSACleanup();
        if (iWSACleanup == SOCKET_ERROR) {
            std::cerr << "WSA cleanup failed & Error Code -> "
                      << getErrorCode(SOCKET_ERROR) << std::endl;
            return SOCKET_ERROR;
        } else {
            std::cout << "WSA cleanup success\n";
            return 0;
        }
    }
#else
    int iClose = close(udpSocketServer);
    if (iClose == SOCKET_ERROR) {
        std::cerr << "Socket closing failed\n";
        return SOCKET_ERROR;
    } else {
        return 0;
    }
#endif
}

//Функция выполняет отправилось ли сообщение клиенту
int userInput(int udpSocketServer, char buffer[], sockaddr_in address,
              int addressLen) {
    int iSendTo = sendto(udpSocketServer, buffer, BUFFER_SIZE, 0,
                         reinterpret_cast<sockaddr*>(&address), addressLen);
    if (iSendTo == SOCKET_ERROR) {
        std::cerr << "Sending failed & Error Code -> "
                  << getErrorCode(SOCKET_ERROR) << std::endl;
        return SOCKET_ERROR;
    } else
        return 0;
}

/* Эта программа выступает в качестве сервера, который принимает по UDP
 * протоколу сообщения от клиентов и отправляет эхо*/
int main() {
    char buffer[BUFFER_SIZE];
    std::string message;

    if (!initWSA()) {
        std::cerr << "WSAStartup failed\n";
        return 1;
    }

    sockaddr_in address = {.sin_family = AF_INET, .sin_port = htons(PORT)};
    address.sin_addr.s_addr = INADDR_ANY;

    // Создание сокета для отправки дэйтаграмм по протоколу UDP
    int udpSocketServer = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (udpSocketServer == INVALID_SOCKET) {
        std::cerr << "Socket creation error & Error Code -> " << INVALID_SOCKET
                  << std::endl;
        return 1;
    }

    // Нужно связать сокет со структурой адреса (IP-адресом и портом)
    int iBind = bind(udpSocketServer, reinterpret_cast<sockaddr*>(&address),
                     sizeof(address));
    if (iBind == SOCKET_ERROR) {
        std::cerr << "Binding failed & Error Code -> "
                  << getErrorCode(SOCKET_ERROR) << std::endl;
        return 1;
    }

    std::cout << "Binding Success\n";

    socklen_t addressLen = sizeof(address);

    // Нужно выполнить выключение сервера по отправке сообщения "EXIT" от
    // клиента
    while (true) {
        // Получение самого сообщения от клиента
        int iReceiveFrom =
            recvfrom(udpSocketServer, buffer, BUFFER_SIZE, 0,
                     reinterpret_cast<sockaddr*>(&address), &addressLen);
        if (iReceiveFrom == SOCKET_ERROR) {
            std::cerr << "Receiving failed & Error Code -> "
                      << getErrorCode(SOCKET_ERROR) << std::endl;
            return 1;
        }
        std::cout << "Buffer - " << buffer << std::endl;

        message = std::string(buffer);

        // Нужно отправить эхо, чтобы клиент понял, что сообщение дошло или
        // выполнилось выключение сервера
        if (iReceiveFrom > 0) {
            if (message == "\\sd server") {
                message = "Server shutdown";
                strcpy(buffer, message.c_str());

                if (userInput(udpSocketServer, buffer, address, addressLen) !=
                    SOCKET_ERROR) {
                    break;
                } else
                    return 1;

            } else {
                if (userInput(udpSocketServer, buffer, address, addressLen) ==
                    SOCKET_ERROR) {
                    return 1;
                }
            }

            std::cout << "Sending Success\n";
        }
    }

    int iCloseSocket = closeSocket(udpSocketServer);
    if (iCloseSocket == SOCKET_ERROR) {
        return 1;
    }
}